import os

import requests

from bitbucket_pipes_toolkit import TokenAuth
from bitbucket_pipes_toolkit.test import PipeTestCase


BITBUCKET_BASE_URL = "https://bitbucket.org"
BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"
ACCOUNT = 'bbcitest'
REPOSITORY = 'test-bitbucket-upload-file'


def delete_file_from_downloads(token, account, repository, filename):
    url = f"{BITBUCKET_API_BASE_URL}/repositories/{account}/{repository}/downloads/{filename}"
    auth = TokenAuth(token)

    return requests.delete(url, auth=auth)


class UploadFileSuccessTestCase(PipeTestCase):
    filename = 'pipe.yml'

    def test_upload_success(self):
        result = self.run_container(
            environment={
                'ACCOUNT': ACCOUNT,
                'REPOSITORY': REPOSITORY,
                'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': self.filename,
            }
        )

        delete_file_from_downloads(
            os.getenv('BITBUCKET_ACCESS_TOKEN'),
            ACCOUNT,
            REPOSITORY,
            self.filename,
        )

        self.assertIn("Successfully uploaded files", result)
        self.assertIn(self.filename, result)

    def test_upload_wildcard_multiple_files_success(self):

        result = self.run_container(
            environment={
                'ACCOUNT': ACCOUNT,
                'REPOSITORY': REPOSITORY,
                'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': '*NG.md',
            }
        )

        delete_file_from_downloads(
            os.getenv('BITBUCKET_ACCESS_TOKEN'),
            ACCOUNT,
            REPOSITORY,
            'CONTRIBUTING.md',
        )
        delete_file_from_downloads(
            os.getenv('BITBUCKET_ACCESS_TOKEN'),
            ACCOUNT,
            REPOSITORY,
            'RELEASING.md',
        )

        self.assertIn("Successfully uploaded files", result)
        self.assertIn('RELEASING.md', result)
        self.assertIn('CONTRIBUTING.md', result)


class UploadFileFailTestCase(PipeTestCase):
    filename = 'pipe.yml'

    def test_fail_no_params(self):
        result = self.run_container()

        self.assertRegex(
            result, r"✖ Validation errors")

    def test_fail_if_file_not_exist(self):
        filename = 'file-not-exist'

        result = self.run_container(
            environment={
                'ACCOUNT': ACCOUNT,
                'REPOSITORY': REPOSITORY,
                'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': filename,
            }
        )

        self.assertRegex(
            result, rf"✖ File {filename} doesn't exist")

    def test_fail_if_wrong_repository(self):
        result = self.run_container(
            environment={
                'ACCOUNT': ACCOUNT,
                'REPOSITORY': 'not-exist-wrong-repo',
                'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': self.filename,
            }
        )

        self.assertIn(f"Failed to upload files {[self.filename]}. Status code: 404.", result)

    def test_fail_if_wrong_account(self):
        result = self.run_container(
            environment={
                'ACCOUNT': 'not-exist-wrong-account',
                'REPOSITORY': REPOSITORY,
                'BITBUCKET_ACCESS_TOKEN': os.getenv('BITBUCKET_ACCESS_TOKEN'),
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': self.filename,
            }
        )

        self.assertIn(f"Failed to upload files {[self.filename]}. Status code: 404.", result)

    def test_fail_if_wrong_bitbucket_app_password(self):
        result = self.run_container(
            environment={
                'ACCOUNT': ACCOUNT,
                'REPOSITORY': REPOSITORY,
                'BITBUCKET_ACCESS_TOKEN': 'wrong-token',
                'BITBUCKET_CLONE_DIR': os.getenv('BITBUCKET_CLONE_DIR'),
                'FILENAME': self.filename,
            }
        )

        self.assertRegex(
            result, r'✖ API request failed with status 401')
