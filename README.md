# Bitbucket Pipe: bitbucket-upload-file

Upload a file from Bitbucket Pipelines to [Bitbucket Downloads](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html).
Simple use cases include uploading an artifact, tar.gz file or storing generated test reports etc.


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-upload-file:0.7.4
  variables:
    FILENAME: '<string>'
    # BITBUCKET_USERNAME: "<string>" # Optional
    # BITBUCKET_APP_PASSWORD: "<string>" # Optional
    # BITBUCKET_ACCESS_TOKEN: '<string>' # Optional
    # ACCOUNT: '<string>' # Optional
    # REPOSITORY: '<string>' # Optional
    # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable                   | Usage                                                                                                                                                                                                                                                       |
|----------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| FILENAME (*)               | Name of the local file to upload. Supports [glob patterns][glob patterns] "\*". e.g. \*.html. Limit max 10 files, if limit reached - build fails. Accepts files only from `BITBUCKET_CLONE_DIR` or any subfolders in this path.                             |
| BITBUCKET_USERNAME (1)     | Bitbucket user that will be used to upload the file. Required unless `BITBUCKET_ACCESS_TOKEN` is used.                                                                                                                                                      |
| BITBUCKET_APP_PASSWORD (1) | [Bitbucket app password][Bitbucket app password] of the user who will be used for authentication. Used with `BITBUCKET_USERNAME`. Required unless `BITBUCKET_ACCESS_TOKEN` is used. Permissions are required: `Repositories write` and `Repositories read`. |
| BITBUCKET_ACCESS_TOKEN (1) | The [access token][Bitbucket access token] will be used for authentication. Required unless `BITBUCKET_USERNAME` and `BITBUCKET_APP_PASSWORD` are used. `Repositories write` and `Repositories read`                                                        |
| ACCOUNT                    | Name of the Bitbucket account in which the file will be uploaded to. If empty, it will be uploaded to the same account where the pipeline runs. Default: `${BITBUCKET_REPO_OWNER}`.                                                                         |
| REPOSITORY                 | Name of the Bitbucket repository in which the file will be uploaded to. If empty, it will be uploaded to the same repository where the pipeline runs. Default: `${BITBUCKET_REPO_SLUG}`.                                                                    |
| DEBUG                      | Flag to turn on extra debug information. Default: `false`.                                                                                                                                                                                                  |

_(*) = required variable._

_(1) = required variable. Required one of the multiple options._


## Prerequisites
To use this pipe, you need to generate an [app password][Bitbucket app password] or an [access token][Bitbucket access token].
Remember to check the `Repositories write` and `Repositories read` permissions when generating the app password. If you want to upload a file to a repository owned by a team account, make sure you have the correct access to the repository.


## Examples

### Basic example

Upload a file to Bitbucket Downloads for the current repository.

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.7.4
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: 'package.json'
```


Upload multiple files by pattern:

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.7.4
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: '*.html'
```

Upload a file to Bitbucket Downloads for the current repository. Authentication method is by `BITBUCKET_ACCESS_TOKEN`.

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.7.4
    variables:
      BITBUCKET_ACCESS_TOKEN: $BITBUCKET_ACCESS_TOKEN
      FILENAME: 'package.json'
```


### Advanced example

Upload a file to Bitbucket Downloads for the other repository in the other account where user has access.

```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.7.4
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: 'package.json'
      ACCOUNT: 'team-account'
      REPOSITORY: 'awesome-repository-name'
```


Upload multiple `txt` files recursively with `**` pattern. Will match any files and zero or more directories, subdirectories and symbolic links to directories.

Note! Using the `**` pattern in large directory trees may consume an inordinate amount of time.
```yaml
script:
  - pipe: atlassian/bitbucket-upload-file:0.7.4
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      FILENAME: '**/*.txt'
```


## Support

If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,downloads,file
[variables-in-pipelines]: https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/
[Bitbucket app password]: https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/
[Bitbucket access token]: https://support.atlassian.com/bitbucket-cloud/docs/access-tokens/
[glob patterns]: https://man7.org/linux/man-pages/man7/glob.7.html
