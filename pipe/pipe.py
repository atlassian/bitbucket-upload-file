import os
import json
import glob
from pathlib import Path

import requests
import yaml
from http import HTTPStatus

from bitbucket_pipes_toolkit import Pipe


MAX_FILES_UPLOAD_LIMIT = 10

BITBUCKET_BASE_URL = "https://bitbucket.org"
BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"

schema = {
    'BITBUCKET_USERNAME': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_APP_PASSWORD': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_ACCESS_TOKEN': {'required': False, 'type': 'string', 'excludes': ['BITBUCKET_USERNAME', 'BITBUCKET_APP_PASSWORD']},
    'FILENAME': {'type': 'string', 'required': True},
    'ACCOUNT': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_OWNER')},
    'REPOSITORY': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_SLUG')},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class UploadFilePipe(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )

        self.files = self.get_variable('FILENAME')
        self.account = self.get_variable('ACCOUNT')
        self.repository = self.get_variable('REPOSITORY')
        self.auth = self.resolve_auth()

    def run(self):

        self.log_info('Executing the pipe...')
        self.log_info(f'Current directory: {Path().absolute()}')
        self.log_warning(f'This pipe accepts files only from {os.environ["BITBUCKET_CLONE_DIR"]} or any subfolders in this path.')

        url = f"{BITBUCKET_API_BASE_URL}/repositories/{self.account}/{self.repository}/downloads"

        # https://meta.stackoverflow.com/questions/368287/canonical-for-why-are-wildcards-not-being-expanded-by-subprocess-call-run-popen
        # wildcard preprocessing before run subprocess
        if "*" in self.files:
            files_list = []
            for file in self.files.split(' '):
                files_list.extend(glob.glob(file, recursive=True))

        else:
            files_list = self.files.split(' ')

        # only uniq values
        files_list = list(set(files_list))

        if not files_list:
            self.fail(f"Files by pattern {self.files} not found.")

        if len(files_list) > MAX_FILES_UPLOAD_LIMIT:
            self.fail(f'Found {len(files_list)} files to upload. Supported max '
                      f'number of files to upload {MAX_FILES_UPLOAD_LIMIT}.')

        multiple_files = []
        for filename in files_list:
            if not Path(filename).is_file():
                self.fail(f"File {filename} doesn't exist.")

            multiple_files.append(('files', (filename, FileOnDemand(filename, 'rb'))))

        file_names = [fname[1][0] for fname in multiple_files]

        self.log_info(f"Start uploading {len(file_names)} files {file_names}...")

        response = requests.post(url, auth=self.auth, files=multiple_files)

        # Bitbucket API returns status code 201 'Created' after success upload
        # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/downloads#post
        if response.status_code == HTTPStatus.CREATED:
            link = f"{BITBUCKET_BASE_URL}/{self.account}/{self.repository}/downloads"
            for filename in file_names:
                basename = os.path.basename(filename)
                self.log_info(f"You can download the file by clicking in this link: {link}/{basename}")

            self.success(f"Successfully uploaded files {file_names} to {link}")
        elif response.status_code == HTTPStatus.UNAUTHORIZED:
            # clarifying message for users
            self.fail(
                "API request failed with status 401. Check your username and app password and try again.")
        else:
            try:
                data = response.json()
            except json.decoder.JSONDecodeError:
                data = None

            message = data['error']['message'] if data and data.get('error') else response.text

            self.fail(
                f"Failed to upload files {file_names}. "
                f"Status code: {response.status_code}. "
                f"Message: {message}."
            )


class FileOnDemand:

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def read(self):
        with open(self.filename, self.mode) as f:
            return f.read()


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())

    pipe = UploadFilePipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
