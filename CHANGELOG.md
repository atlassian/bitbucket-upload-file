# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.7.4

- patch: Internal maintenance: Added logs and warnings about directory from upload.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 0.7.3

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.7.2

- patch: Fix an example in the readme.

## 0.7.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update tests to authenticate only via bitbucket access token.

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 0.4.0

- minor: Add support for token based authentication.
- patch: Internal maintenance: update links to Bitbucket Pipelines documentation.
- patch: Internal maintenance: update pipe versions in pipeline config file.

## 0.3.4

- patch: Updated README: added clarification about FILENAME variable usage.

## 0.3.3

- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update pipe dependencies.
- patch: Internal maintenance: update pipeline execution process.
- patch: Internal maintenance: update tests.

## 0.3.2

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.3.1

- patch: Fix max files limit.

## 0.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.2.0

- minor: Add support for uploading multiple files.

## 0.1.8

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.1.7

- patch: Internal maintenance: remove version digest from image

## 0.1.6

- patch: Internal maintenance: fix version parsing

## 0.1.5

- patch: Internal maintenance: Update release process.

## 0.1.4

- patch: Internal maintenance: Add gitignore secrets.

## 0.1.3

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.1.2

- patch: Add warning message about new version of the pipe available.

## 0.1.1

- patch: Internal maintenance: update pipes toolkit version

## 0.1.0

- minor: Initial release
